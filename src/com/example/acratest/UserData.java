package com.example.acratest;

import java.util.UUID;

import android.app.Activity;
import android.content.Context;
import android.telephony.TelephonyManager;

public class UserData extends Thread {

	

	public static String getLogin(Activity act) {
		

			final TelephonyManager tm = (TelephonyManager) act.getBaseContext()
					.getSystemService(Context.TELEPHONY_SERVICE);

			final String tmDevice, tmSerial, androidId, operatorID, operatorName, phoneNum, country;
			tmDevice = "" + tm.getDeviceId();
			tmSerial = "" + tm.getSimSerialNumber();

			phoneNum = "" + tm.getLine1Number();
			country = "" + tm.getNetworkCountryIso();
			operatorID = "" + tm.getNetworkOperator();
			operatorName = "" + tm.getNetworkOperatorName();

			androidId = ""
					+ android.provider.Settings.Secure.getString(
							act.getContentResolver(),
							android.provider.Settings.Secure.ANDROID_ID);

			UUID deviceUuid = new UUID(androidId.hashCode(),
					((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
			String deviceId = deviceUuid.toString();

			// System.out.println("+++" + tmDevice + "<>" + tmSerial + "<>"
			// + androidId + "<>" + deviceId);
			return "+++" + phoneNum + "<>" + country + "<>" + operatorID + "<>"
					+ operatorName + "<>" + tmDevice + "<>" + tmSerial + "<>"
					+ androidId + "<>" + deviceId;
		
	}
}

package com.example.acratest;

import name.khartn.ihtika.client.VarsStrorage.Storage;

import org.acra.ACRA;
import org.acra.ACRAConfiguration;
import org.acra.ACRAConfigurationException;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

import android.app.Activity;
import android.app.Application;

@ReportsCrashes(formKey = "testetstets",				
				resToastText = R.string.crash_toast_text, // optional, displayed as soon as the crash occurs, before collecting data which can take a few seconds
				resDialogText = R.string.crash_dialog_text,
				resDialogIcon = android.R.drawable.ic_dialog_info, //optional. default is a warning sign
				resDialogTitle = R.string.crash_dialog_title, // optional. default is your application name
				resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, // optional. when defined, adds a user text field input with this text resource as a label
				resDialogOkToast = R.string.crash_dialog_ok_toast // optional. displays a Toast message when the user accepts to send a report.
) 
public class Crtest extends Application {
	public void onCreate() {
		
		Storage.appContext=getApplicationContext();
		
        ACRA.init(this);
        
        
        try {
        	ACRAConfiguration config = ACRA.getConfig();
			config.setMode(ReportingInteractionMode.DIALOG);
			ACRA.setConfig(config);
		} catch (ACRAConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
        MySender mySender=new MySender();
        ACRA.getErrorReporter().setReportSender(mySender);
        
        super.onCreate();
    }
}
